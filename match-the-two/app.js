'use strict';

const cards = 
[
    {
        name: 'fries',
        img: 'images/fries.png',
    },
    {
        name: 'cheeseburger',
        img: 'images/cheeseburger.png',
    },
    {
        name: 'hotdog',
        img: 'images/hotdog.png',
    },
    {
        name: 'milkshake',
        img: 'images/milkshake.png',
    },
    {
        name: 'ice-cream',
        img: 'images/ice-cream.png',
    },
    {
        name: 'pizza',
        img: 'images/pizza.png',
    },
    {
        name: 'fries',
        img: 'images/fries.png',
    },
    {
        name: 'cheeseburger',
        img: 'images/cheeseburger.png',
    },
    {
        name: 'hotdog',
        img: 'images/hotdog.png',
    },
    {
        name: 'milkshake',
        img: 'images/milkshake.png',
    },
    {
        name: 'ice-cream',
        img: 'images/ice-cream.png',
    },
    {
        name: 'pizza',
        img: 'images/pizza.png',
    }
];

cards.sort(() => 0.5 - Math.random());

const gridDisplay = document.querySelector('#grid'),
      resultDisplay = document.querySelector('#result'),
      cardsGuessed = [];

let cardsPicked = [],
    cardsPickedIds = [];

function createBoard()
{
    for (let i = 0; i < cards.length; i += 1)
    {
        const card = document.createElement('img');
        card.setAttribute('src', 'images/blank.png')
        card.setAttribute('data-id', i);
        card.addEventListener('click', flipCard);
        gridDisplay.appendChild(card);
    }
}

createBoard();

function checkMatch ()
{
    const allCards = document.querySelectorAll('img'),
          optionOneId = cardsPickedIds[0],
          optionTwoId = cardsPickedIds[1];


    if(optionOneId == optionTwoId)
    {
        allCards[optionOneId].setAttribute('src', 'images/blank.png');
        allCards[optionTwoId].setAttribute('src', 'images/blank.png');
        alert('You clicked the same image!');
    }

    if(cardsPicked[0] == cardsPicked[1])
    {
        alert("It's a match!");
        allCards[optionOneId].setAttribute('src', 'images/white.png');
        allCards[optionTwoId].setAttribute('src', 'images/white.png');
        allCards[optionOneId].removeEventListener('click', flipCard);
        allCards[optionTwoId].removeEventListener('click', flipCard);

        cardsGuessed.push(cardsPicked);
    }
    else
    {
        allCards[optionOneId].setAttribute('src', 'images/blank.png');
        allCards[optionTwoId].setAttribute('src', 'images/blank.png');
        alert("Try again.");
    }

    resultDisplay.textContent = cardsGuessed.length;
    cardsPicked = [];
    cardsPickedIds = [];

    if(cardsGuessed.length == cards.length/2)
    {
        resultDisplay.textContent = 'Great, you guessed them all! To play again, press F5.';
    }
}

function flipCard ()
{
    const cardId = this.getAttribute('data-id');
    cardsPicked.push(cards[cardId].name)
    cardsPickedIds.push(cardId)

    this.setAttribute('src', cards[cardId].img);

    if (cardsPicked.length === 2)
    {
        setTimeout(checkMatch, 500)
    }
}
'use strict';

const computerChoiceDisplay = document.getElementById('computer-choice'),
      userChoiceDisplay = document.getElementById('user-choice'),
      resultDisplay = document.getElementById('result'),
      possibleChoices = document.querySelectorAll('button');

let userChoice,
    computerChoice,
    result;

possibleChoices.forEach(possibleChoice => possibleChoice.addEventListener('click', (e) => 
{
  userChoice = e.target.id;
  userChoiceDisplay.innerHTML = userChoice;
  generateComputerChoice();
  getResult();
}))

function generateComputerChoice() 
{
  const randomNumber = Math.floor(Math.random() * possibleChoices.length) + 1
  
  if (randomNumber === 1) 
  {
    computerChoice = 'Rock'
  }
  if (randomNumber === 2) 
  {
    computerChoice = 'Scissors'
  }
  if (randomNumber === 3) 
  {
    computerChoice = 'Paper'
  }
  computerChoiceDisplay.innerHTML = computerChoice
}

const getResult = () => 
{
    switch (userChoice + computerChoice) 
    {
      case 'ScissorsPaper':
      case 'RockScissors':
      case 'PaperRock':
        resultDisplay.innerHTML = "You win!"
        break
      case 'PaperScissors':
      case 'ScissorsRock':
      case 'RockPaper':
        resultDisplay.innerHTML = "You lose!"
        break
      case 'PaperPaper':
      case 'ScissorsScissors':
      case 'RockRock':
      resultDisplay.innerHTML = "Draw!"
      break
    }
  }